package com.page.locators;

import org.openqa.selenium.By;

public interface AmazonShirtPageLocators {
	
	public static By SelectedItem=By.xpath(".//*[@id='result_0']/div/div[2]/div[1]/a/h2");
	public static By SHIRT_SIZE=By.id("native_dropdown_selected_size_name");
	public static By ADD_TO_CART_BTN=By.id("add-to-cart-button");
	public static By ADDED_TO_CART_MSG=By.xpath("//div[@id='huc-v2-order-row-confirm-text']/h1");
	

}
