package com.page.locators;

import org.openqa.selenium.By;

public interface AmazonHomeLocators 
{
  public static By SEARCH_FIELD=By.id("searchDropdownBox");
  public static By GO_BTN=By.xpath(".//*[@id='nav-search']/form/div[2]/div/input");
  public static By MEN=By.xpath(".//*[@id='nav-subnav']/a[3]");
  public static By SHIRTS=By.linkText("Shirts");
}
