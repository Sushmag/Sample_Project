package com.page.testsuite;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.page.module.AmazonHomePage;
import com.page.module.AmazonShirtsPage;

public class TestAddItemToCart {
	
	private WebDriver driver;
	AmazonHomePage homePage;
	AmazonShirtsPage shirtsPage;
	
	@BeforeMethod
	public void doSetUp()
	{
		String path=System.getProperty("user.dir")+"\\Resources\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",path);
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.in");
	}
	@Test
	public void testAddItemToCart() throws Exception
	{
		homePage=new AmazonHomePage(driver);
		homePage.searchforShoesAndHandbag();
		//shirtsPage=new AmazonShirtsPage(driver);
		shirtsPage=homePage.naviagteToShirtsPage();
		shirtsPage.SelectItem();
		shirtsPage.windowhandle();
		shirtsPage.SelectSizeAndAddtoCart();
		shirtsPage.ValidateMessage();
	  
	}
	
	@Test
	public void SelectBrandandVerifyResults()
	{
		
	}
	
	@AfterMethod
	public void undoSetUp()
	{
		
	}

}
