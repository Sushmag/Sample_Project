package com.page.module;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.page.locators.AmazonHomeLocators;

public class AmazonHomePage implements AmazonHomeLocators{
	private WebDriver driver;
	
	public AmazonHomePage(WebDriver driver){
		this.driver=driver;
	}
	
	public void searchforShoesAndHandbag()
	{
		System.out.println("hello");
		WebElement we = driver.findElement(SEARCH_FIELD);
		Select dropdown=new Select(we);
		dropdown.selectByVisibleText("Shoes & Handbags");
		driver.findElement(GO_BTN).click();
	}
	
	
	public  AmazonShirtsPage naviagteToShirtsPage() throws Exception
	{
		Actions itemname=new Actions(driver);
		WebElement Myitem=driver.findElement(MEN);
		itemname.moveToElement(Myitem).build().perform();
		Thread.sleep(1000);
		driver.findElement(SHIRTS).click();
		return new AmazonShirtsPage (driver);
	}
	
	

}
