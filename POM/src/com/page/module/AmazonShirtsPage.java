package com.page.module;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.page.locators.AmazonShirtPageLocators;

public class AmazonShirtsPage implements AmazonShirtPageLocators 
{
	private WebDriver driver;
	
	public AmazonShirtsPage(WebDriver driver){
		this.driver=driver;
	}
	

 public void SelectItem()
 {
	 driver.findElement(SelectedItem).click();
	 
	 
 }
 
 public void windowhandle(){
	 String Parentwindow=driver.getWindowHandle();
	 Set<String> windows=driver.getWindowHandles();
	 
	 for(String Window:windows)
	 {
		 if(!windows.equals(Parentwindow))
		 {
			 driver.switchTo().window(Window);
		 }
	 }
 }
 
 public void SelectSizeAndAddtoCart() throws Exception
 {
	 	WebElement Shirtsize= driver.findElement(SHIRT_SIZE);
		Select MyShirtsize=new Select(Shirtsize);
		MyShirtsize.selectByVisibleText("Large");
		Thread.sleep(1000);
		driver.findElement(ADD_TO_CART_BTN).click();
		//isElementClickable(driver,ADD_TO_CART_BTN);
	 
 }
 
 /*public static boolean isElementClickable(WebDriver driver,By locator)
 {
 	boolean b = false;
 	WebDriverWait wait = new WebDriverWait(driver,200);
 	try
 	{
 	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
 	element.click();
 	b=true;
 	}	
 	catch(Exception e){
 		
 	}
 	return b;
 }*/
 
 public void ValidateMessage()
 {
	 	WebElement Addtocart=driver.findElement(ADDED_TO_CART_MSG);
		String textmsg=Addtocart.getText();
		System.out.println(textmsg);
		Assert.assertEquals(textmsg,"Added to Cart");
 }
 
}